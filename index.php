<?php
require_once "libraries/vendor/autoload.php";
require_once "libraries/crawler.php";
require_once "libraries/PorterStemmer.php";
require_once "preprocessing.php";
require_once "mongo.php";
require_once "vectorModel.php";
require_once "QueryingService.php";
require_once "Timer.php";
require_once "fileupload.php";
ini_set('memory_limit', '-1');
?>


<?php
$preproccesed = false;
$uploader;
$collection = '';
$res = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //insert methods that need to be run when querying
    if (isset($_POST['Search'])) {
        $collection = $_POST['collection'];
        $rawQuery = $_POST['query'];

        // parse query by dividing per ',' delimiter
        $queryArray = explode(",", $rawQuery);

        // create result array
        $resultQueryArray = array();

        // iterate over items of query
        foreach ($queryArray as $queryItem) {
            //split one item in half by ':'  and check whether contains weight value
            $splitQueryItem = explode(":", $queryItem);
            if (count($splitQueryItem) > 1) {
                // there is weight so assign it into result array
                $resultQueryArray = array_merge($resultQueryArray, [PorterStemmer::Stem(mb_strtolower(trim($splitQueryItem[0]))) => (int) trim($splitQueryItem[1])]);
            } else {
                // set default weight into result array
                $resultQueryArray = array_merge($resultQueryArray, [PorterStemmer::Stem(mb_strtolower(trim($queryItem))) => 1]);
            }
        }
        // call the query service
        $mongoObject = new mongo();
        if (isset($_POST['searchMode'])) {
            $resultMongo = $mongoObject->getCollection($collection)->findOne([],
                ['projection' => ['terms' => 1]]);

            $array = iterator_to_array($resultMongo);
            $timer = new Timer('Query Timer');
            $queryResult = QueryingService::query($array, $resultQueryArray);
        } else {
            $resultMongo = $mongoObject->getCollection($collection)->findOne([],
                ['projection' => ['raw_documents' => 1]]);

            $array = iterator_to_array($resultMongo);
            $timer = new Timer('Query Timer');
            $queryResultNaive = QueryingService::queryNaive($array, $resultQueryArray);
        }
        $resultTime = $timer->finish();
    }
    //insert methods that need to be run when inserting documents via url
    if (isset($_POST['URL_Insert'])) {
        $collection = $_POST['URL'];
        $preprocessingObject = new Preprocessing($collection, $_POST['URLMaxPages']);
        $documents = $preprocessingObject->preproccess();
        $preproccesed = true;
        unset($_POST['URL_Insert']);
    }
    //insert methods that need to be run when inserting documents via zip insert
    if (isset($_POST['ZIP_insert'])) {
        $uploader = new fileUploader();
        $collection = basename($_FILES['fileToUpload']["name"], ".zip");
        $library = $uploader->unzipAndProcess();
        $preprocessingObject = new Preprocessing("file", 3, true);
        $documents = $preprocessingObject->preproccess($library);
        $preproccesed = true;
    }
    unset($_REQUEST);
    $_REQUEST = NULL;
}
if ($preproccesed) {
    $vectorModelObject = new vectorModel($documents, $collection);
    $vectorModelObject->createTermByDocumentMatrix();
    $vectorModelObject->createTermByDocumentMatrixWeighted();
    $vectorModelObject->insertMatrixIntoDB();
}

$mongoObject = new mongo();
$collections = $mongoObject->listCollections();
if (!empty($collection)) {
    $res = $mongoObject->getCollection($collection)->findOne([],
        ['projection' => ['terms' => 1, '_id' => 0, 'terms' . 'term' => 1]]);
}

?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Home - Vector Model</title>
        <link rel="stylesheet" href="public/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cabin:700">
        <link rel="stylesheet" href="public/assets/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="public/assets/css/styles.min.css">
        <style>
            .toggle.rounded-custom, .toggle-on.rounded-custom, .toggle-off.rounded-custom { border-radius: 20rem; width:80px }
            .toggle.rounded-custom .toggle-handle { border-radius: 20rem; width:10px}
        </style>
    </head>

    <body id="page-top" style="background-color: rgba(0,0,0,0.39);">
        <nav class="navbar navbar-light navbar-expand-md navbar navbar-expand-lg fixed-top" id="mainNav">
            <div class="container"><a class="navbar-brand js-scroll-trigger" href="#">Vector Model</a><button
                    data-toggle="collapse" class="navbar-toggler navbar-toggler-right" data-target="#navbarResponsive"
                    type="button" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"
                    value="Menu"><i class="fa fa-bars"></i></button>
            </div>
        </nav>
        <header class="masthead" style="background-image:url('public/assets/img/intro-bg.jpg');">
            <div class="intro-body" style="height: 700px;">
                <section>
                    <div>
                        <div>
                            <form id="query" method="POST" action="">
                            <h4 style="margin-top: 50px;margin-bottom: 30px;">Choose Collection of documents</h4>
                                <table style="margin-left:auto;margin-right:auto;">
                                    <tr>
                                        <td style="text-align:left;">
                                            <label style="margin: 0px;padding: 0px;">Select Collection: </label>
                                        </td>
                                        <td style="text-align:left;">
                                            <select class="custom-select chosen" required="" name="collection" style="color: #232323;width: 482px;margin: 10px;">
                                            <?php
foreach ($collections as $col) {
    print('<option value="' . $col['name'] . '">' . $col['name'] . '</option>');
}
?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <label style="margin: 0px;padding: 0px;">Query: </label>
                                        </td>
                                        <td style="text-align:left;">
                                            <input type="text" name = "query" style="margin: 15px;width: 300px;height: 38px;" placeholder="enter query">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <label style="margin: 0px;padding: 0px;">Select Query mode: </label>
                                        </td>
                                        <td style="text-align:right;">
                                            <input name="searchMode" value="mode" data-style="rounded-custom" type="checkbox" data-size="sm" checked data-toggle="toggle" data-on="Vector Search" data-off="sequential Search" data-onstyle="success" data-offstyle="danger">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input class="btn btn-primary" name = "Search" type="submit" style="padding: 6px 12px; text-align: center;" value="Search"></input>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <div id="insertWrapper" style ="display: none;">
                            <h4 style="margin-top: 50px;margin-bottom: 30px;">Choose how to get documents and procces</h4>
                                <form id="insertURL" action="" method="POST">
                                        <table style="margin-left:auto;margin-right:auto;">
                                            <tr>
                                                <td style="text-align:left;">
                                                    <label for="URLT">URL in format begining in http/s: </label>
                                                </td>
                                                <td style="text-align:left;">
                                                    <input type = "text" id = "URLT" name="URL"></input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;">
                                                    <label for="URLN">Max number of pages to download: </label>
                                                </td>
                                                <td style="text-align:left;">
                                                    <input type = "number" id = "URLN" name="URLMaxPages"></input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <input class="btn btn-primary" name = "URL_Insert" type="submit" style="padding: 6px 12px; text-align: center;" value="URL Insert"></input>
                                                </td>
                                            </tr>
                                        </table>
                                </form>
                                <form id="insertZIP" action="" method="POST" style = "display: none;" enctype="multipart/form-data">
                                        <table style="margin-left:auto;margin-right:auto;">
                                            <tr>
                                                <td style="text-align:left;">
                                                    <label for="URLT">ZIP of documents in txt format</label>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td style="text-align:left;">
                                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                                </td>
                                             </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <button class="btn btn-primary" name = "ZIP_insert" type="submit" style="padding: 6px 12px; text-align: center;">Upload</button>
                                                </td>
                                            </tr>
                                        </table>
                                </form>
                                <form id="insert" action="" method="POST">
                                    <input  class="btn btn-primary" type ="button" id="insert" name="SelectorInsert" value="Change mode">
                                </form>
                            </div>
                            <br>
                            <form action="" method="POST" id="selector">
                                <input  class="btn btn-primary" type ="button" id="selector" name="selector" value="Change mode">
                            </form>
                        </div>
                        <?php
                            if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['Search'])) {
                        ?>
                                <div style="margin-bottom: 20px;margin-top: 20px;">
                                <table style="margin-left:auto;margin-right:auto;">
                                    <tr>
                                        <td style="text-align:left">
                                            <h6 style="display:inline;">Collection:</h6>
                                        </td>
                                        <td style="text-align:left">
                                            <span style="margin-left: 10px;"> <?php echo $collection ?> </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left">
                                            <h6 style="display:inline;">Query:</h6>
                                        </td>
                                        <td style="text-align:left">
                                            <span style="margin-left: 10px;"> <?php echo $rawQuery ?> </span>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                                <div>
                                    <?php
                                        // print out the result
                                        echo '<h6>Resulted documents</h6>';
                                        echo '<ol>';
                                        if ($queryResult) {
                                            foreach ($queryResult as $queryResultItem) {
                                                echo '<li><span>' . $queryResultItem->doc_name . '</span></li>';
                                            }
                                        } else if ($queryResultNaive) {
                                            foreach ($queryResultNaive as $queryResultItem) {
                                                echo '<li><span>' . $queryResultItem->doc_name . '</span></li>';
                                            }
                                        }
                                        echo '</ol>';

                                        print_r('<small>Query took ' . $resultTime * 1000 . ' milliseconds.</small>');
                                    ?>
                                </div>
                            <?php
                            }
                            ?>
                </section>
            </div>
        </header>
        <div class="map-clean"></div>
        <footer>
            <div class="container text-center">
                <p>Copyright ©&nbsp;Vector Model 2020</p>
            </div>
        </footer>
        <script src="public/assets/js/jquery.min.js"></script>
        <script src="public/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script src="public/assets/js/script.min.js"></script>
        <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
        <script>
            $(function() {
                $("#selector").click(function()
                {
                    $('#query').toggle();
                    $('#insertWrapper').show();
                    $('#selector').hide();
                });
                $("#insert").click(function()
                {
                    $('#insertZIP').toggle();
                    $('#insertURL').toggle();
                    $('#insert').hide();
                });
            });
        </script>
    </body>

</html>
