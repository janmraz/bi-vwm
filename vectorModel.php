<?php
require_once "DocumentWrapper.php";

class vectorModel
{
    private $documents = array();
    private $collection = '';
    private $matrix = array();
    private $termByDocumentMatrix = array(array());
    private $termByDocumentMatrixWeighted = array(array());
    private $numOfTerms = 0;
    private $numOfDocs = 0;

    public function __construct($documents, $collection)
    {
        $this->documents = $documents;
        $this->collection = $collection;
        $this->numOfDocs = count($documents);

        foreach ($this->documents as $document) {
            ///creating matrix from the documents. This will be the base of the term by weight matrix
            ///
            /// This is one big function that recursivly merges the documents into a structure that aids
            /// the creation of the matrix
            $this->matrix = array_merge_recursive
                (
                $this->matrix,
                array_combine
                (
                    array_keys
                    (
                        $document->getData()
                    ),
                    array_map
                    (
                        function ($item) use ($document) {
                            return $document->getName() . "::" . $item;
                        },
                        array_values
                        (
                            $document->getData()
                        )
                    )
                )
            );
        }
        // print("<pre>".print_r($this -> matrix,true)."</pre>");
    }

    public function createTermByDocumentMatrix()
    {
        $this->termByDocumentMatrix = array
            (
            "terms" => array
            (
            ),
            "raw_documents" => $this->documents,
        );
        $i = 0;
        foreach ($this->matrix as $key => $value) {
            array_push($this->termByDocumentMatrix["terms"], array("term" => $key, "value" => array()));
            if (is_array($value)) {
                foreach ($value as $item) {
                    $parts = explode('::', $item);
                    array_push($this->termByDocumentMatrix["terms"][$i]["value"], array("doc_name" => $parts[0], "weight" => $parts[1]));
                }
            } else {
                $parts = explode('::', $value);
                array_push($this->termByDocumentMatrix["terms"][$i]["value"], array("doc_name" => $parts[0], "weight" => $parts[1]));
            }
            $i++;
        }
    }

    public function createTermByDocumentMatrixWeighted()
    {
        $this->termByDocumentMatrixWeighted = $this->termByDocumentMatrix;
        foreach ($this->termByDocumentMatrixWeighted["terms"] as &$term) {
            foreach ($term["value"] as &$doc) {
                $doc["weight"] = ($doc["weight"] / max(array_column($term["value"], "weight"))) * log($this->numOfDocs / count(array_column($term["value"], "weight")), 2);
            }
        }
        //print("<pre>".print_r($this -> termByDocumentMatrixWeighted,true)."</pre>");
    }

    public function insertMatrixIntoDB()
    {
        $mongoObject = new mongo();
        $mongoObject->insert($this->collection, $this->termByDocumentMatrixWeighted);
    }

    public function printMatrix()
    {
        echo "<br>";
        echo "<table style='border: solid 1px black;'>";
        echo "<tr style='border: solid 1px black;'>";
        $i = 0;
        foreach ($this->termByDocumentMatrix as $row) {
            $j = 0;
            if ($i != 0) {
                echo "<tr style='border: solid 1px black;'>";
            }

            foreach ($row as $item) {
                echo "<td style='border: solid 1px black;'>";
                echo $item;
                echo "</td>";
            }
            if ($i != count($this->documents)) {
                echo "</tr>";
            }
            $i++;
        }
    }

    public function printMatrixWeighted()
    {
        echo "<br>";
        echo "<table style='border: solid 1px black;'>";
        echo "<tr style='border: solid 1px black;'>";
        $i = 0;
        foreach ($this->termByDocumentMatrixWeighted as $row) {
            $j = 0;
            if ($i != 0) {
                echo "<tr style='border: solid 1px black;'>";
            }

            foreach ($row as $item) {
                echo "<td style='border: solid 1px black;'>";
                echo $item;
                echo "</td>";
            }
            if ($i != count($this->documents)) {
                echo "</tr>";
            }
            $i++;
        }
    }

    public function printTerms()
    {
        echo "<h3><b>Terms unweighted</b></h3>";
        $termsUnweighted = $this->termByDocumentMatrix[0];
        foreach ($termsUnweighted as $term) {
            echo $term . "<br>";
        }
        echo "<br>";
        echo "<h3><b>Terms weighted</b></h3>";
        $termsWeighted = $this->termByDocumentMatrixWeighted[0];
        foreach ($termsWeighted as $term) {
            echo $term . "<br>";
        }
    }
}
