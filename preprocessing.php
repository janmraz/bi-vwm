<?php
require_once "libraries/PorterStemmer.php";
require_once "DocumentWrapper.php";
use voku\helper\HtmlDomParser;

/********************************************//**
 *PREPROCESSING
 * this class does all the preprocessing of a collection of documents that are passed by url (later maybe by zip file)
 * the main procces is:
 *      Crawl the website until max number of pages is reached
 *      Srub through the list and clear out non working links
 *      Extract only words from the pages
 *      Stem them
 *      Count number of different terms in page
 *      Unify the term list (that means we want only how many times a single term is in the document)
 * Outputs the documents in an array
 ***********************************************/

class Preprocessing
{
    private $url;
    private $numLinks;
    private $parseLinksObject;

    private $linkArray = array();
    private $library = array();
    private $words = array();
    private $frequency = array();
    private $clearedWords = array();
    private $terms = array();
    private $documents = array();

    public function __construct($baseUrl, $numLinks, $file = false)
    {
        if (!$file) {
            $this->url = $baseUrl;
            $this->numLinks = $numLinks;
            $this->parseLinksObject = new ParseLinks($this->url, $this->numLinks);
        }

    }

    public function printLinks()
    {
        $this->parseLinksObject->printLinks();
    }

    private function crawler()
    {

        $this->linkArray = $this->parseLinksObject->getAllLinks();
        $this->cleanURLList();
    }

    private function cleanURLList()
    {
        reset($this->linkArray);
        foreach ($this->linkArray as $key => $one) {
            if (stripos($one, 'jpg') !== false ||
                stripos($one, 'jpeg') !== false ||
                stripos($one, 'png') !== false ||
                stripos($one, 'svg') !== false ||
                stripos($one, 'user') !== false) {
                unset($this->linkArray[$key]);
                continue;
            }

            $file_headers = @get_headers($one);
            if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                unset($this->linkArray[$key]);
            }
        }
        array_splice($this->linkArray, 0, 0);
    }

    public function outputHTML()
    {
        if (empty($this->linkArray)) {
            $this->crawler();
        }
        foreach ($this->linkArray as $L) {

            $doc = new DOMDocument();
            $doc->formatOutput = true;

            $url = parse_url($L);
            $name = $url['host'] . str_replace("/", "_", $url['path']);
            $nameHTML = "html/" . $name . ".html";
            @$doc->loadHTML(file_get_contents($L));

            file_put_contents($nameHTML, $doc->saveHTML());
        }
    }

    public function outputXML()
    {
        if (empty($this->linkArray)) {
            $this->crawler();
        }
        foreach ($this->linkArray as $key => $L) {

            $url = parse_url($L);
            $name = $url['host'] . str_replace("/", "_", $url['path']);
            $nameXML = "xml/" . $name . ".xml";
            try
            {
                $dom = HtmlDomParser::file_get_html($L);
            } catch (Exception $e) {
                unset($this->linkArray[$key]);
                continue;
            }
            foreach ($dom->find('img') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('head') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('svg') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('script') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('comment') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('a') as $e) {
                if (strpos($e->href, "instagram")) {
                    $e->outertext = '';
                }
            }
            foreach ($dom->find('a') as $e) {
                if (strpos($e->href, "#")) {
                    $e->outertext = '';
                }
            }
            foreach ($dom->find('*[class]') as $e) {
                $e->removeAttribute('class');
            }
            $document = $dom->save();

            $tidy = new tidy();
            $config = array('indent' => true,
                'output-xml' => true,
                'hide-comments' => true,
                'wrap' => 400,
            );
            $document = $tidy->repairString($document, $config);

            $document = str_replace("&nbsp;", " ", $document);

            file_put_contents($nameXML, $document);
            array_splice($this->linkArray, 0, 0);
        }
    }

    public function outputALL()
    {
        $this->crawler();
        $this->outputHTML();
        $this->outputXML();
    }

    function print() {
        echo "<div style='width:50%;float:left;'>";
        echo "<h2>HTML FILES</h2>";
        foreach ($this->linkArray as $L) {
            $url = parse_url($L);
            $name = $url['host'] . str_replace("/", "_", $url['path']);
            $nameHTML = "html/" . $name . ".html";
            echo "<a href=" . $nameHTML . ">" . $L . "</a>";
            echo "<br>";
        }
        echo "</div>";
        echo "<div style='width:50%;float:right;'>";
        echo "<h2>XML FILES</h2>";
        foreach ($this->linkArray as $L) {
            $url = parse_url($L);
            $name = $url['host'] . str_replace("/", "_", $url['path']);
            $nameXML = "xml/" . $name . ".xml";
            echo "<a href=" . $nameXML . ">" . $L . "</a>";
            echo "<br>";
        }
        echo "</div>";
    }

    public function plainText()
    {
        if (empty($this->linkArray)) {
            $this->crawler();
        }
        $doc = new DOMDocument();
        $doc->formatOutput = true;
        foreach ($this->linkArray as $L) {
            try
            {
                $dom = HtmlDomParser::file_get_html($L);
            } catch (Exception $e) {
                unset($this->linkArray[$key]);
                continue;
            }
            foreach ($dom->find('img') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('head') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('script') as $e) {
                $e->outertext = '';
            }
            foreach ($dom->find('comment') as $e) {
                $e->outertext = '';
            }
            $url = parse_url($L);
            //echo $url['path'];
            $doc = new DOMDocument();
            $doc->formatOutput = true;
            @$doc->loadHTML(file_get_contents($L));
            $text = strip_tags($dom->save());
            $test = array(str_replace("/", "_", $url['path']), $text);
            array_push($this->library, $test);
        }
        return $this->library;
    }

    public function Stemming()
    {
        foreach ($this->library as $document) {
            $wordS = array();
            $wordCleared = preg_replace('/[^\w]/u', ' ', $document[1]);
            $bagOfWords = preg_split('/\s+/', $wordCleared);
            // echo "<strong> Bag Of Words: </strong>";
            // echo "<br>";
            // print_r($bagOfWords);
            foreach ($bagOfWords as $word) {
                array_push($wordS, PorterStemmer::Stem(mb_strtolower($word)));
            }
            $tmp = array($document[0], $wordS);
            array_push($this->words, $tmp);
        }
        return $this->words;
    }
    /// This function converts crawled pages to stuctured array of documents the structure is as follows.
    /// output of preprocess is an array which contains documents. Documents are an array consisting of name and array of stemmed words.
    /// In pseudocode the structre is [doc1[], doc2[], doc3[]] where doc1 is [name, words[]]
    public function preproccess($library = array())
    {
        if (empty($library)) {
            $this->plainText();
        } else {
            $this->library = $library;
        }
        $this->Stemming();
        $this->CountFrequency();
        $this->removeCommonWords();
        $this->createUniqueTermArray();
        $this->transformToDocuments();
        return $this->documents;
    }

    public function CountFrequency()
    {
        foreach ($this->words as $document) {
            $tmp = array($document[0], array_count_values($document[1]));

            array_push($this->frequency, $tmp);
        }
    }

    public function removeCommonWords()
    {
        $tmpArray = array();
        foreach ($this->frequency as $frequencyItem) {
            array_push($tmpArray, $frequencyItem[1]);
        }
        $sharedValues = count($tmpArray) > 1 ? call_user_func_array('array_intersect_key', $tmpArray) : $tmpArray;
        foreach ($this->frequency as $frequencyItem) {
            $tmp = array($frequencyItem[0], array_diff_key($frequencyItem[1], $sharedValues));
            array_push($this->clearedWords, $tmp);
        }
    }

    public function createUniqueTermArray()
    {
        $tmpArray = array();
        foreach ($this->clearedWords as $document) {
            foreach ($document[1] as $key => $item) {
                array_push($tmpArray, $key);
            }
        }
        $this->terms = array_unique($tmpArray);
    }

    public function getTerms()
    {
        return $this->terms;
    }

    public function transformToDocuments()
    {
        foreach ($this->clearedWords as $document) {
            array_push($this->documents, new DocumentWrapper($document[1], $document[0], array_sum($document[1])));
        }
    }

};
