<?php
//ini_set('memory_limit','-1');

require_once "mongo.php";

/**
 * Class QueryResult
 */
class QueryResult
{
    /**
     * @var document name
     */
    public $doc_name;
    /**
     * @var cos distance from query vector
     */
    public $cosDistance;

    public function __construct($doc_name, $cosDistance)
    {
        $this->doc_name = $doc_name;
        $this->cosDistance = $cosDistance;
    }

}
/**
 * Class QueryResultNaive
 */
class QueryResultNaive
{
    /**
     * @var document name
     */
    public $doc_name;
    /**
     * @var frequency array
     */
    public $frequencies;

    public function __construct($doc_name, array $frequencies)
    {
        $this->doc_name = $doc_name;
        $this->frequencies = $frequencies;
    }

}

/**
 * Class QueryingService is responsible for retrieving results from db
 *
 * TODO trick with json_decode and json_encode to get rid of MongdDb objects but only plain php ones
 * TODO better structure for quicker querying if object would be [{"doc_name: value"}] (using hash table to access at almost instant time)
 */
class QueryingService
{

    /**
     * Counts cos distance of two vectors represented as array of numbers
     *
     * @param array $arr1 eg. [1,2,3]
     * @param array $arr2 eg. [2.1,1,3]
     * @return float|int value of cos distance
     */
    private static function countCosDistance(array $arr1, array $arr2)
    {
        $arr1SquaredSum = QueryingService::sumSquareRoots($arr1);
        $arr2SquaredSum = QueryingService::sumSquareRoots($arr2);

        $innerProduct = 0;
            foreach ($arr1 as $key => $value)
            {
                $innerProduct += $value * $arr2[$key];
            }
        $dividedBy = sqrt($arr1SquaredSum * $arr2SquaredSum);

        $result = $innerProduct / $dividedBy;
//        if(is_infinite($result)){
//            self::log($arr1SquaredSum);
//            self::log($arr1);
//        }

        return $result;
    }

    private static function sumSquareRoots(array $numbers)
    {
        $total = 0;
        foreach ($numbers as  $term => $number) {
            $sqrt = $number * $number;
            $total += $sqrt;
        }
        return $total;
    }

    /**
     * Flatten the array to the depth of 1 max
     *
     * @param array $array
     * @return array
     */
    private static function flatten(array $array)
    {
        $return = array();
        array_walk_recursive($array, function ($a) use (&$return) {$return[] = $a;});
        return $return;
    }

    public static function queryNaive($array, $input)
    {
        $queryVector = array_values($input);
        $queryTermNames = array_keys($input);

//        print_r("<div>internal log</div>");
        //        print_r("<div>input:</div>");
        //        print_r(($input));
        //        print_r(($queryTermNames));
        //        print_r(($queryVector));

        $result = array();

        // iterate over documents
        foreach ($array["raw_documents"] as $document_wrapper) {
            // create document vector for storing weights for this document
            $frequencies = array();
            // iterate over terms
            foreach ($document_wrapper->data as $termName => $termFrequency) {
                // find appropriate query term
                foreach ($queryTermNames as $queryName) {
                    if ($queryName === $termName) {
                        // add frequency into list
                        $frequencies[$queryName] = $termFrequency;
                    }
                }
            }
            // create result object if there are any frequencies
            if (!empty($frequencies)) {
                array_push($result, new QueryResultNaive($document_wrapper["name"], $frequencies));
            }

        }

        // sort results based on count of matching terms and frequency values
        usort($result, function ($a, $b) {
            if (count($a->frequencies) == count($b->frequencies)) {
                return array_sum($a->frequencies) < array_sum($b->frequencies);
            }
            return count($a->frequencies) < count($b->frequencies);
        });

        // get first 10 items
        $result = array_slice($result, 0, 10);
//        self::log($result);
        return $result;
    }

    private static function log($string)
    {
        echo '<pre>';
        var_dump($string);
        echo '</pre>';
    }

    /**
     * @param $input
     * @param $link
     */
    public static function query($array ,$input)
    {
        $queryVector = array_values($input);
        $queryTermNames = array_keys($input);

//        print_r("<div>internal log</div>");
        //        print_r("<div>input:</div>");
        //        print_r(($input));
        //        print_r(($queryTermNames));
        //        print_r(($queryVector));

        $result = array();

        // filter given array to only ones applicable
        $filteredTerms = self::filterTerms($array["terms"], $queryTermNames);

        // attach names as array of strings in term object
        $termsWithDocumentsNames = self::getTermsWithArrayOfDocumentNames($filteredTerms);

        // get all document names
        $allNames = array_merge(QueryingService::flatten(array_map(function ($term_wrapper) {return $term_wrapper->docs;}, $termsWithDocumentsNames)));
        $documentNames = array_unique($allNames);

//        print_r("<div>documentNames:</div>");
        //        print_r(json_encode($documentNames));

        // iterate over documents
        foreach ($documentNames as $documentName) {
            // create document vector for storing weights for this document
            $documentVector = array();
            // iterate over terms
            foreach ($termsWithDocumentsNames as $termWithDocumentsNames) {
                // check if exists if not set 0
                if (in_array($documentName, $termWithDocumentsNames->docs)) {
                    // else find his weight
                    foreach ($termWithDocumentsNames->value as $document_wrapper) {
                        if ($document_wrapper->doc_name == $documentName) {
                            // set weight
                            $documentVector[$termWithDocumentsNames->term] = $document_wrapper->weight;
                            break;
                        }
                    }
                }
            }
            // create result object
            array_push($result, new QueryResult($documentName, self::countCosDistance($documentVector, $input)));
        }

        // sort by the closest documents
        usort($result, function ($a, $b) {
            return $a->cosDistance < $b->cosDistance;
        });

//        print_r("<div>result:</div>");
        //        print_r($result);

//        self::log($result);
        $result = array_slice($result, 0, 10);
        return $result;
    }

    /**
     * Filter terms based on given array of keys. Method returns only terms which are present in given array of keys
     *
     * @param array $terms array of objects e.g. [{term: "term_name", ...}, ...]
     * @param array $input array of keys e.g ["key1", "key2"]
     * @return array filtered array of objects ($terms)
     */
    public static function filterTerms($terms, array $input): array
    {
        $filteredTerms = array();
        foreach ($terms as $term_wrapper) {
            if (in_array($term_wrapper->term, $input, true)) {
                array_push($filteredTerms, json_decode(json_encode($term_wrapper)));
            }
        }
        return $filteredTerms;
    }

    /**
     * Gets terms enriched with field documents which contains all string names of documents included in the term object
     *
     * @param array $filteredTerms of terms objects e.g. [{term: "term_name", ...}, ...]
     * @return array enriched input array of objects with field docs
     */
    public static function getTermsWithArrayOfDocumentNames(array $filteredTerms): array
    {
        $termsWithDocumentsNames = array_map(function ($term_wrapper) {
            $term_wrapper->docs = array_map(function ($doc) {
//                print_r("</br>doc:");
                //                print_r(json_encode($doc));
                return $doc->doc_name;
            }, json_decode(json_encode($term_wrapper->value)));
            return $term_wrapper;
        }, $filteredTerms);
        return $termsWithDocumentsNames;
    }

}

// test function to be called - fish:30, pig, yellowstone, geyser, lake, ocean
//QueryingService::query(["fish" => 1, "pig" => 1, "yellowstone" => 1, "lake" => 1, "ocean" => 1], "nature");