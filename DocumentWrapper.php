<?php

class DocumentWrapper
{
    public $data = array();
    public $name;
    public $totalCount;

    public function __construct($data, $name, $totalCount)
    {
        $this->name = $name;
        $this->data = $data;
        $this->totalCount = $totalCount;
    }

    public function printData()
    {
        foreach ($this->data as $key => $item) {
            echo $key . ": " . $item . "<br/>";
        }
    }

    public function printAll()
    {
        echo "Document: <strong>" . $this->name . "</strong><br/>";
        echo "<div>Total count of terms: <strong>" . $this->totalCount . "</strong></div><br/>";
        $this->printData();
        echo "<br/><br/>";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getData()
    {
        return $this->data;
    }
    public function getTotalCount()
    {
        return $this->totalCount;
    }
}
