<?php
require_once "preprocessing.php";

class fileUploader
{
    private $targetDir = 'uploads/';
    private $targetFile;
    private $fileName;

    public function __construct()
    {
        $this->targetFile = $targetFile = $this->targetDir . basename($_FILES['fileToUpload']["name"]);
        $this->fileName = pathinfo($targetFile, PATHINFO_FILENAME);

        $fileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        if ($fileType != 'zip') {
            echo "File type not supported";
            return;
        }
        if ($_FILES["fileToUpload"]["size"] > 500000000) {
            echo "Sorry, your file is too large.";
        }
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
            echo "Upload completed";
        } else {
            echo "Upload failed";
        }
    }

    public function unzipAndProcess()
    {
        mkdir($this->targetDir . $this->fileName);
        $path = $this->targetDir . $this->fileName;
        $zip = new ZipArchive;
        $res = $zip->open($this->targetFile);
        if ($res === true) {
            $zip->extractTo($path);
            $zip->close();
        }
        $fileList = glob("uploads/" . $this->fileName . "/*.txt");
        $library = array();
        foreach ($fileList as $file) {
            $test = array(basename($file, ".txt"), file_get_contents($file));
            array_push($library, $test);
        }
        return $library;
        unlink($path);
        unlink($his->$targetFile);
    }
}
