<?php 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

set_time_limit(15000);
class ParseLinks
{
    private $sRootLink;
    private $iCountOfPages;

    private $linkArray = array();
    private $iDeep;
    private $sDomain;
    private $sScheme;

public function __construct($sRootLink, $iCountOfPages)
{
    $this->sRootLink = $sRootLink;
    $this->iCountOfPages = $iCountOfPages;
    $this->iDeep = 0;
    $this->sDomain = "";
    $this->sScheme = "";
}

public function getAllLinks()
{
    array_push($this -> linkArray, $this -> sRootLink);
    $this->recParseLinks($this->sRootLink);
    //$this->saveToCSV();
    return $this->linkArray;
}

public function printLinks()
{
    echo "Web-site: www." . $this->sDomain . "</br>Max link count: " . count($this->linkArray) . "</br></br>";
    // foreach($this->linkArray as $element)
    //     echo "<a href=\"" . $element . "\">" . $element . "</a>" . "<br>";
}

private function saveToCSV()
{
    $fp = fopen("allLinksFromYourSite.csv", "w");
    fwrite($fp, "Web-site: $this->sDomain" . PHP_EOL);
    fwrite($fp, "Count of links: " . count($this->linkArray) . PHP_EOL . PHP_EOL);
    foreach($this->linkArray as $element)
        fwrite($fp, $element . PHP_EOL);
    fclose($fp);
}

private function recParseLinks($link)
{
    if(strlen($link) <= 1)
        return;

    if($this->iDeep == 0)
    {
        $d = parse_url($link);

        if($d != false)
        {
            $this->sDomain = $d['host'];
            $this->sScheme = $d['scheme'];
        }
        else
            return;
    }

    $this->iDeep++;
    $doc = new DOMDocument();
    $internalErrors = libxml_use_internal_errors(true);
    @$doc->loadHTML(file_get_contents($link));
    $elements = $doc->getElementsByTagName('a');

    foreach($elements as $element)
    {
        if(count($this->linkArray) >= $this->iCountOfPages)
            return;

        $links = $element->getAttribute('href');
        if(!empty($links[0]) && ($links[0] == '/' || $links[0] == '?'))
            $links = $this->sScheme . "://" . $this->sDomain . $links;

        $p_links = parse_url($links);
        if($p_links == FALSE)
            continue;

        if(!isset($p_links["host"]) || $p_links["host"] != $this->sDomain)
            continue;
//exclude img links
        if(!$this->linkExists($links) && strlen($links) > 1)
        {
            $this->linkArray[] = $links;

            if($this->iDeep < 4)
            {
                $this->recParseLinks($links);
            }
        }
    }
    $this->iDeep--;
}

private function linkExists($link)
{
    foreach($this->linkArray as $element)
        if($element == $link)
            return true;

    return false;
}




}
?>
