<?php
require_once 'libraries/vendor/autoload.php';

class mongo
{
    private $host = "mongodb+srv://user:dPN8GLJu8VcPUtcn@vwm-vmqja.gcp.mongodb.net/test?retryWrites=true&w=majority";
    private $database = "documents";
    private $client;
    private $db;

    public function __construct()
    {
        $this->client = new MongoDB\Client($this->host);
        $this->db = $this->client->selectDatabase($this->database);
    }

    public function insert($collection, $doc)
    {
        $collection = $this->client->selectCollection($this->database, $collection);
        $collection->insertOne($doc);
    }
    //wrapper to access collection for running mongoDB commands
    public function getCollection($collection)
    {
        return $this->client->selectCollection($this->database, $collection);
    }

    //wrapper to list collections
    public function listCollections()
    {
        return $this->client->documents->listCollections();
    }

};
