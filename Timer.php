<?php

/**
 * Class Timer
 *
 * it can be used for continuous monitoring of app
 * based on https://stackoverflow.com/a/6245978
 *
 */
class Timer
{
    /**
     *
     * @var string
     */
    private $name;
    /**
     * Representing unix timestamp of beginning of timer
     * @var float|string
     */
    private $start;

    /**
     * Monitoring constructor.
     * @param $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->start = microtime(true);
    }

    /**
     * Finishes current timer and returns time elapsed
     *
     * @return float|string
     */
    public function finish()
    {
        $time_elapsed_secs = microtime(true) - $this->start;
        // TODO log on lower level logger (logger.info or something like this)
        return $time_elapsed_secs * 1000;
    }

    /**
     * Finishes current timer, returns time elapsed and set it from the beginning
     *
     * @return float|string
     */
    public function finishAndRestart()
    {
        $time_elapsed_secs = microtime(true) - $this->start;
        $this->start = microtime(true);
        return $time_elapsed_secs * 1000;
    }
}
